import unittest
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.datasets import make_blobs
from unittest.mock import patch
from src.plotting import (
    plot_k_distance,
    plot_silhouette_scores_per_eps,
    plot_pca, plot_pca_outliers,
    plot_pca_cluster_centers_shift,
    plot_silhouette_graph,
    plot_feature_importances,
    plot_centroid_heatmap,
    plot_feature_distributions,
    plot_features_pairwise,
    plot_sankey_diagram
)


class TestPlottingFunctions(unittest.TestCase):

    def setUp(self):
        self.X, self.labels_true = make_blobs(n_samples=300, centers=4, cluster_std=0.60, random_state=0)
        self.df = pd.DataFrame(self.X, columns=['Feature1', 'Feature2'])
        self.df['Cluster'] = self.labels_true

        self.df_pca = self.df.copy()
        self.df_pca['PCA1'] = self.df['Feature1']
        self.df_pca['PCA2'] = self.df['Feature2']

        self.membership = np.random.rand(len(self.df), 4)
        self.outlier_threshold = 0.2

    @patch("src.plotting.plt.show")
    def test_plot_k_distance(self, mock_show):
        distances = np.sort(np.random.rand(100))
        k = 5
        plot_k_distance(distances, k)
        plt.close()

    @patch("src.plotting.plt.show")
    def test_plot_silhouette_scores_per_eps(self, mock_show):
        eps_values = np.linspace(0.1, 1.0, 10)
        min_samples = 5
        silhouette_scores = np.random.rand(10)
        plot_silhouette_scores_per_eps(eps_values, min_samples, silhouette_scores)
        plt.close()

    @patch("src.plotting.plt.show")
    def test_plot_pca(self, mock_show):
        plot_pca(self.df_pca)
        plt.close()

    @patch("src.plotting.plt.show")
    def test_plot_pca_outliers(self, mock_show):
        max_membership = np.max(self.membership, axis=1)
        plot_pca_outliers(self.df_pca, max_membership, self.outlier_threshold)
        plt.close()

    @patch("src.plotting.plt.show")
    def test_plot_pca_cluster_centers_shift(self, mock_show):
        self.df_pca['ID'] = ['t1'] * 150 + ['t2'] * 150
        plot_pca_cluster_centers_shift(self.df_pca)
        plt.close()

    @patch("src.plotting.plt.show")
    def test_plot_silhouette_graph(self, mock_show):
        silhouette_values = np.random.rand(300)
        silhouette_avg = silhouette_values.mean()
        plot_silhouette_graph(silhouette_values, silhouette_avg, self.df['Cluster'], "Test Label")
        plt.close()

    @patch("src.plotting.plt.show")
    def test_plot_feature_importances(self, mock_show):
        feature_importances = pd.DataFrame({
            'Feature': ['Feature1', 'Feature2'],
            'Importance': [0.6, 0.4]
        })
        plot_feature_importances(feature_importances)
        plt.close()

    @patch("src.plotting.plt.show")
    def test_plot_centroid_heatmap(self, mock_show):
        centroid_distances = np.random.rand(4, 4)
        plot_centroid_heatmap(centroid_distances, 4)
        plt.close()

    @patch("src.plotting.plt.show")
    def test_plot_feature_distributions(self, mock_show):
        cols_to_exclude = 1
        plot_feature_distributions(self.df, self.df['Cluster'], cols_to_exclude)
        plt.close()

    @patch("src.plotting.plt.show")
    def test_plot_features_pairwise(self, mock_show):
        cols_to_exclude = 1
        plot_features_pairwise(self.df, self.df['Cluster'], cols_to_exclude)
        plt.close()

    @patch("src.plotting.go.Figure.show")
    def test_plot_sankey_diagram(self, mock_show):
        df_transition = pd.DataFrame({
            'Cluster_t1': np.random.randint(0, 4, 100),
            'Cluster_t2': np.random.randint(0, 4, 100),
            'transition_rate': np.random.rand(100),
            'count': np.random.randint(1, 10, 100)
        })
        plot_sankey_diagram(df_transition, 4)


if __name__ == '__main__':
    unittest.main()
