import unittest
import pandas as pd
from src.preprocessing import (
    normalize_data, 
    one_hot_encode_data, 
    generate_weighted_groups
)


class TestPreprocessingFunctions(unittest.TestCase):

    def test_normalize_data(self):
        # Create a sample DataFrame
        data = {
            'A': [1, 2, 3, 4, 5],
            'B': [10, 20, 30, 40, 50],
            'C': [100, 200, 300, 400, 500],
            'D': ['ignore1', 'ignore2', 'ignore3', 'ignore4', 'ignore5']
        }
        df = pd.DataFrame(data)

        # Columns to ignore
        cols_to_ignore = ['D']

        # Expected result after normalization
        expected_data = {
            'A': [0.0, 0.25, 0.5, 0.75, 1.0],
            'B': [0.0, 0.25, 0.5, 0.75, 1.0],
            'C': [0.0, 0.25, 0.5, 0.75, 1.0]
        }
        expected_df = pd.DataFrame(expected_data)

        # Call the function
        result_df = normalize_data(df, cols_to_ignore)

        # Assert the result is as expected
        pd.testing.assert_frame_equal(result_df, expected_df)

    def test_one_hot_encode_data(self):
        # Create a sample DataFrame
        data = {
            'A': ['cat', 'dog', 'fish', 'mouse'],
            'B': ['no', 'yes', 'no', 'yes'],
            'C': [1, 2, 3, 4]
        }
        df = pd.DataFrame(data)

        # Categorical columns to one-hot encode
        categorical_cols = ['A', 'B']

        # Expected result after one-hot encoding
        expected_data = {
            'A1': [1.0, 0.0, 0.0, 0.0],
            'A2': [0.0, 1.0, 0.0, 0.0],
            'A3': [0.0, 0.0, 1.0, 0.0],
            'A4': [0.0, 0.0, 0.0, 1.0],
            'B1': [1.0, 0.0, 1.0, 0.0],
            'B2': [0.0, 1.0, 0.0, 1.0]
        }
        expected_df = pd.DataFrame(expected_data)

        # Call the function
        result_df = one_hot_encode_data(df, categorical_cols)

        # Assert the result is as expected
        pd.testing.assert_frame_equal(result_df, expected_df)

    def test_generate_weighted_groups(self):
        # Create a sample DataFrame
        data = {
            'A1': [1, 2, 3, 4],
            'A2': [2, 3, 4, 5],
            'B1': [5, 6, 7, 8],
            'B2': [6, 7, 8, 9]
        }
        df = pd.DataFrame(data)

        # Thematic groups dictionary
        thematic_groups_dict = {
            'GroupA': ['A1', 'A2'],
            'GroupB': ['B1', 'B2']
        }

        # Expected result after generating weighted groups
        expected_data = {
            'GroupA_A1': [0.70710678, 1.41421356, 2.12132034, 2.82842712],
            'GroupA_A2': [1.41421356, 2.12132034, 2.82842712, 3.53553391],
            'GroupB_B1': [3.53553391, 4.24264069, 4.94974747, 5.65685425],
            'GroupB_B2': [4.24264069, 4.94974747, 5.65685425, 6.36396103]
        }
        expected_df = pd.DataFrame(expected_data)

        # Call the function
        result_df = generate_weighted_groups(df, thematic_groups_dict)

        # Assert the result is as expected
        pd.testing.assert_frame_equal(result_df, expected_df)


if __name__ == '__main__':
    unittest.main()
