import unittest
import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from src.modeling import (
    calc_nneighbors_distances,
    find_best_dbscan_params,
    calc_dbscan_clusters,
    calc_fcm_clusters,
    calc_pca
)


class TestModelingFunctions(unittest.TestCase):

    def test_calc_nneighbors_distances(self):
        data = {
            'x': [1, 2, 3, 4, 5],
            'y': [1, 2, 3, 4, 5]
        }
        df = pd.DataFrame(data)
        n_neighbors = 2

        result = calc_nneighbors_distances(df, n_neighbors)
        expected = np.array([1.41421356, 1.41421356, 1.41421356, 1.41421356])

        np.testing.assert_almost_equal(result[:4], expected, decimal=5)

    def test_find_best_dbscan_params(self):
        data = {
            'x': [1, 2, 3, 4, 5, 8, 9, 10],
            'y': [1, 2, 3, 4, 5, 8, 9, 10]
        }
        df = pd.DataFrame(data)
        eps_values = [0.5, 1, 1.5]
        min_samples_list = [2, 3]

        silhouette_scores, eps_list, minpts_list = find_best_dbscan_params(df, eps_values, min_samples_list)

        # Check the length of results
        self.assertEqual(len(silhouette_scores), 6)
        self.assertEqual(len(eps_list), 6)
        self.assertEqual(len(minpts_list), 6)

    def test_calc_dbscan_clusters(self):
        data = {
            'x': [1, 2, 3, 4, 5, 8, 9, 10],
            'y': [1, 2, 3, 4, 5, 8, 9, 10]
        }
        df = pd.DataFrame(data)
        best_eps = 1.5
        best_min_samples = 2

        result = calc_dbscan_clusters(df, best_eps, best_min_samples)
        expected = np.array([0, 0, 0, 0, 0, 1, 1, 1])

        np.testing.assert_array_equal(result, expected)

    def test_calc_fcm_clusters(self):
        data = {
            'x': [1, 2, 3, 4, 5, 8, 9, 10],
            'y': [1, 2, 3, 4, 5, 8, 9, 10]
        }
        df = pd.DataFrame(data)
        n_clusters = 2
        m = 2.0

        clusters, cluster_centers, max_membership, fpc = calc_fcm_clusters(df.T, n_clusters, m)

        # Check the length of results
        self.assertEqual(len(clusters), 2)
        self.assertEqual(cluster_centers.shape, (2, 8))

    def test_calc_pca_with_fitting(self):
        data = {
            'x': [1, 2, 3, 4, 5, 8, 9, 10],
            'y': [1, 2, 3, 4, 5, 8, 9, 10]
        }
        df = pd.DataFrame(data)
        clusters = np.array([0, 0, 0, 0, 0, 1, 1, 1])
        pca = PCA(n_components=2)

        pca_df = calc_pca(df, pca, clusters, perform_fitting=True)

        self.assertEqual(pca_df.shape, (df.shape[0], 3))
        self.assertIn('PCA1', pca_df.columns)
        self.assertIn('PCA2', pca_df.columns)
        self.assertIn('Cluster', pca_df.columns)
        self.assertTrue(np.all(pca_df['Cluster'].values == clusters))

    def test_calc_pca_without_fitting(self):
        data = {
            'x': [1, 2, 3, 4, 5, 8, 9, 10],
            'y': [1, 2, 3, 4, 5, 8, 9, 10]
        }
        df = pd.DataFrame(data)
        clusters = np.array([0, 0, 0, 0, 0, 1, 1, 1])
        pca = PCA(n_components=2)
        pca.fit(df)

        pca_df = calc_pca(df, pca, clusters, perform_fitting=False)

        self.assertEqual(pca_df.shape, (df.shape[0], 3))
        self.assertIn('PCA1', pca_df.columns)
        self.assertIn('PCA2', pca_df.columns)
        self.assertIn('Cluster', pca_df.columns)
        self.assertTrue(np.all(pca_df['Cluster'].values == clusters))


if __name__ == '__main__':
    unittest.main()
