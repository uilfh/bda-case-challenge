import unittest
import numpy as np
import pandas as pd
from sklearn.datasets import make_blobs
from src.evaluation import (
    calc_cluster_distances,
    calc_avg_intra_cluster_distances,
    calc_intra_cluster_distances,
    calc_centroid_distances,
    calc_silhouette_score,
    calc_dunn_index,
    calc_fuzzy_dunn_index,
    calc_feature_importances,
    calc_anova,
    calc_transition_counts,
    calc_mult_cluster_distances,
    calc_linear_sum_assignment
)


class TestEvaluationFunctions(unittest.TestCase):

    def setUp(self):
        self.X, self.labels_true = make_blobs(n_samples=300, centers=4, cluster_std=0.60, random_state=0)
        self.df = pd.DataFrame(self.X, columns=['Feature1', 'Feature2'])
        self.df['Cluster'] = self.labels_true

        # Cluster centers for testing
        self.cluster_centers = np.array([[0, 0], [1, 1], [2, 2], [3, 3]])

    def test_calc_cluster_distances(self):
        cluster_labels_unique = self.df['Cluster'].unique()
        distances = calc_cluster_distances(self.df, cluster_labels_unique, ['Feature1', 'Feature2'])

        self.assertTrue('simple' in distances)
        self.assertTrue('complete' in distances)
        self.assertTrue('average' in distances)
        self.assertTrue(len(distances['simple']) > 0)

    def test_calc_avg_intra_cluster_distances(self):
        cluster_labels_unique = self.df['Cluster'].unique()
        distances = calc_avg_intra_cluster_distances(self.df, cluster_labels_unique, ['Feature1', 'Feature2'])

        self.assertTrue(isinstance(distances, dict))
        self.assertTrue(len(distances) > 0)

    def test_calc_intra_cluster_distances(self):
        n_clusters = 4
        distances, max_distances = calc_intra_cluster_distances(self.df, self.cluster_centers, n_clusters)

        self.assertTrue(len(distances) == n_clusters)
        self.assertTrue(len(max_distances) == n_clusters)

    def test_calc_centroid_distances(self):
        n_clusters = 4
        distances, distances_dict = calc_centroid_distances(self.cluster_centers, n_clusters)

        self.assertTrue(distances.shape == (n_clusters, n_clusters))
        self.assertTrue(len(distances_dict) > 0)

    def test_calc_silhouette_score(self):
        silhouette_values, silhouette_avg = calc_silhouette_score(self.df, self.df['Cluster'], ['Feature1', 'Feature2'])

        self.assertTrue(len(silhouette_values) == len(self.df))
        self.assertTrue(silhouette_avg > 0)

    def test_calc_dunn_index(self):
        cluster_labels = self.df['Cluster'].unique()
        dunn_index = calc_dunn_index(self.df, cluster_labels)

        self.assertTrue(dunn_index > 0)

    def test_calc_fuzzy_dunn_index(self):
        centroid_distances, _ = calc_centroid_distances(self.cluster_centers, len(self.cluster_centers))
        max_intra_cluster_distances = [1, 2, 3, 4]
        fuzzy_dunn_index = calc_fuzzy_dunn_index(centroid_distances, max_intra_cluster_distances)

        self.assertTrue(fuzzy_dunn_index > 0)

    def test_calc_feature_importances(self):
        importances_df = calc_feature_importances(self.df)

        self.assertTrue('Feature' in importances_df.columns)
        self.assertTrue('Importance' in importances_df.columns)
        self.assertTrue(len(importances_df) > 0)

    def test_calc_anova(self):
        anova_df = calc_anova(self.df)

        self.assertTrue('Feature' in anova_df.columns)
        self.assertTrue('F-statistic' in anova_df.columns)
        self.assertTrue('p-value' in anova_df.columns)
        self.assertTrue(len(anova_df) > 0)

    def test_calc_transition_counts(self):
        df1 = self.df.copy()
        df2 = self.df.copy()
        df1['unique_id'] = range(len(df1))
        df2['unique_id'] = range(len(df2))

        transition_counts = calc_transition_counts(df1, df2)

        self.assertTrue('Cluster_t1' in transition_counts.columns)
        self.assertTrue('Cluster_t2' in transition_counts.columns)
        self.assertTrue('count' in transition_counts.columns)
        self.assertTrue('transition_rate' in transition_counts.columns)

    def test_calc_mult_cluster_distances(self):
        cluster_centers_t1 = self.cluster_centers
        cluster_centers_t2 = self.cluster_centers + 1

        distances_dict, distance_matrix = calc_mult_cluster_distances(cluster_centers_t1, cluster_centers_t2)

        self.assertTrue(distance_matrix.shape[0] == len(cluster_centers_t1))
        self.assertTrue(distance_matrix.shape[1] == len(cluster_centers_t2))
        self.assertTrue(len(distances_dict) > 0)

    def test_calc_linear_sum_assignment(self):
        distance_matrix = np.random.rand(4, 4)

        matching = calc_linear_sum_assignment(distance_matrix)

        self.assertTrue(isinstance(matching, dict))
        self.assertTrue(len(matching) > 0)


if __name__ == '__main__':
    unittest.main()
