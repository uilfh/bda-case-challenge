# Exploring Complex Patterns - Group 2 - Focus on Fuzzy C-Means

## Group Members: 
- Mert Aras, 2514174
- Stefan Horst, 2510445
- Mike Hutfilz, 1764276
- Thanh Nga Nguyen, 1943244
- Lazaros Tampakis, 2210884

## Reproducibility

Necessary steps for executing our code:

**Operating System**: Windows

**Python Version**: 3.11.4

**Environment Setup**: 
````
conda create –n bda python=3.11.4
conda activate bda
pip install –r requirements.txt
pip install .
````

**Main Entry Point**
````
See notebooks
````

**Unittest & docstring coverage**:
````
pytest --cov-report term --cov=src tests/
docstr-coverage src -i -f
````  


## Project Organization
------------
```
    ├── README.md 							<-- this file. insert group members here
    ├── .gitignore 						    <-- prevents you from submitting several clutter files
    ├── data
    │   ├── modeling
    │   │   ├── dev 						<-- your development set goes here
    │   │   ├── test 						<-- your test set goes here
    │   │   └── train 						<-- your train set goes here goes here
    │   ├── preprocessed 					<-- your preprocessed data goes here
    │   └── raw								<-- the provided raw data for modeling goes here
    ├── docs								<-- provided explanation of raw input data goes here
    │
    ├── models								<-- dump models here
    ├── presentation                        <-- please submit your presentation in this folder
    ├── notebooks							<-- your playground for juptyer notebooks
    ├── requirements.txt 					<-- required packages to run your submission (use a virtualenv!)
    ├── src
           ├── additional_features.py 			<-- your creation of additional features/data goes here
           ├── predict.py 						<-- your prediction script goes here
           ├── preprocessing.py 				<-- your preprocessing script goes here
           ├── train.py 						<-- your training script goes here
           └── demo.py                       <-- demo script
    └── tests
           └── test_demo.py                  <-- demo script for unittest                
	
```
## Code evaluation

To evaluate your code, we will run the following commands:

````
pytest --cov-report term --cov=src tests/
docstr-coverage src -i -f
````