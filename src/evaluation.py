import numpy as np
import pandas as pd
from sklearn.metrics import silhouette_score, silhouette_samples
from sklearn.ensemble import RandomForestClassifier
from scipy.spatial.distance import cdist, pdist, squareform
from scipy.stats import f_oneway
from scipy.optimize import linear_sum_assignment
import warnings


def calc_cluster_distances(df_input, cluster_labels_unique, data_columns):
    """
    Calculates the average, minimum and maximum distances between clusters.

    Parameters:
    df_input: Input data as Pandas DataFrame, contains cluster membership
    cluster_labels_unique: Unique cluster labels
    data_columns: Columns of the data used to calculate the distances

    Returns:
    cluster_distances: Dictionary with simple, complete and average distances between clusters
    """
    cluster_distances = {'simple': {}, 'complete': {}, 'average': {}}
    for i, cluster1 in enumerate(cluster_labels_unique):
        for cluster2 in cluster_labels_unique[i + 1:]:
            points1 = df_input[df_input['Cluster'] == cluster1][data_columns]
            points2 = df_input[df_input['Cluster'] == cluster2][data_columns]
            distances = cdist(points1, points2, metric='euclidean')

            min_distance = np.min(distances)
            cluster_distances['simple'][(cluster1, cluster2)] = min_distance
            max_distance = np.max(distances)
            cluster_distances['complete'][(cluster1, cluster2)] = max_distance
            avg_distance = np.mean(distances)
            cluster_distances['average'][(cluster1, cluster2)] = avg_distance
            
    return cluster_distances


# DBSCAN
def calc_avg_intra_cluster_distances(df_input, cluster_labels_unique, data_columns):
    """
    Calculates the average distances within clusters.

    Parameters:
    df_input: Input data as Pandas DataFrame, contains cluster membership
    cluster_labels_unique: Unique cluster labels
    data_columns: Columns of the data used to calculate the distances

    Returns:
    intra_cluster_distances: Dictionary with average distances within the clusters
    """
    intra_cluster_distances = {}
    for cluster in cluster_labels_unique:
        points = df_input[df_input['Cluster'] == cluster][data_columns]
        if len(points) > 1:
            distances = pdist(points, metric='euclidean')
            avg_distance = np.mean(distances)
        else:
            avg_distance = 0 # When the Cluster contains only 1 element, then the distance is 0
        intra_cluster_distances[cluster] = avg_distance

    return intra_cluster_distances


# FCM
def calc_intra_cluster_distances(df_input, cluster_centers, n_clusters):
    """
    Calculates the average and maximum distances within clusters to their centroids.

    Parameters:
    df_input: Input data as Pandas DataFrame, contains cluster membership
    cluster_centers:  Centroids of the clusters
    n_clusters: Number of clusters

    Returns:
    intra_cluster_distances: List of the average distances within the clusters
    max_intra_cluster_distances: List of the maximum distances within the clusters
    """
    intra_cluster_distances = []
    max_intra_cluster_distances = [] # required for Fuzzy Dunn Index
    for i in range(n_clusters):
        cluster_points = df_input[df_input['Cluster'] == i].drop('Cluster', axis=1).values
        center = cluster_centers[i]
        distances = cdist(cluster_points, [center])

        avg_distance = distances.mean()
        intra_cluster_distances.append(avg_distance)
        max_distance = distances.max()
        max_intra_cluster_distances.append(max_distance)

    return intra_cluster_distances, max_intra_cluster_distances


# FCM
def calc_centroid_distances(cluster_centers, n_clusters):
    """
    Calculates the distances between cluster centroids.

    Parameters:
    clustaer_centers: Centroids of the clusters
    n_clusters: Number of clusters

    Returns:
    centroid_distances: Matrix of the distances between the centroids
    centroid_distances_dict: Dictionary with the distances between the centroids
    """
    centroid_distances = cdist(cluster_centers, cluster_centers)
    centroid_distances_dict = {(cluster1, cluster2): 
                               centroid_distances[cluster1, cluster2] 
                               for cluster1 in range(n_clusters) 
                               for cluster2 in range(cluster1 + 1, n_clusters)}

    return centroid_distances, centroid_distances_dict


def calc_silhouette_score(df_input, cluster_labels, data_columns):
    """
    Calculates the silhouette score for clusters.

    Parameters:
    df_input: Input data as Pandas DataFrame, contains cluster memberships
    cluster_labels: Cluster labels for the data
    data_columns: Columns of the data used to calculate the Silhouette score

    Returns:
    silhouette_values: Silhouette values for each data point
    silhouette_avg: Average silhouette score for the entire dataset
    """
    silhouette_values = silhouette_samples(df_input[data_columns], cluster_labels)
    silhouette_avg = silhouette_score(df_input[data_columns], cluster_labels)

    return silhouette_values, silhouette_avg


# DBSCAN
def calc_dunn_index(df_input, cluster_labels):
    """
    Calculates the Dunn index for clusters.

    Parameters:
    df_input: Input data as Pandas DataFrame, contains cluster membership
    cluster_labels: Unique cluster labels

    Returns:
    dunn_index: Calculated Dunn index
    """
    if len(cluster_labels) < 2: # Dunn-Index is not defined for less than 2 clusters 
        return 0

    inter_cluster_distances = []
    intra_cluster_distances = []

    for cluster in cluster_labels:
        cluster_points = df_input[df_input['Cluster'] == cluster]
        if len(cluster_points) > 1:
            distances = pdist(cluster_points)
            intra_cluster_distances.append(np.max(distances))
    
    for cluster1 in cluster_labels:
        for cluster2 in cluster_labels:
            if cluster1 >= cluster2:
                continue
            points1 = df_input[df_input['Cluster'] == cluster1]
            points2 = df_input[df_input['Cluster'] == cluster2]
            distances = np.min(squareform(pdist(np.vstack((points1, points2))))[:len(points1), len(points1):])
            inter_cluster_distances.append(distances)
    
    min_inter_cluster_distance = np.min(inter_cluster_distances)
    max_intra_cluster_distance = np.max(intra_cluster_distances)

    return min_inter_cluster_distance / max_intra_cluster_distance


# FCM
def calc_fuzzy_dunn_index(centroid_distances, max_intra_cluster_distances):
    """
    Calculates the fuzzy Dunn index for clusters.

    Parameters:
    centroid_distances: Matrix of distances between the centroids
    max_intra_cluster_distances: List of maximum distances within the clusters

    Returns:
    fuzzy_dunn_index: Calculated fuzzy Dunn index
    """
    min_inter_cluster_distance = np.min(centroid_distances[np.nonzero(centroid_distances)])
    max_intra_cluster_distance = np.max(max_intra_cluster_distances)
    fuzzy_dunn_index = min_inter_cluster_distance / max_intra_cluster_distance 

    return fuzzy_dunn_index


def calc_feature_importances(df_input):
    """
    Calculates the importance of the features using a random forest classifier.

    Parameters:
    df_input: Input data as Pandas DataFrame, contains cluster membership

    Returns:
    importances_df: Panda's DataFrame with the features and their importance
    """
    X = df_input.drop('Cluster', axis=1)
    y = df_input['Cluster']

    rf = RandomForestClassifier(n_estimators=100, random_state=1)
    rf.fit(X, y)

    feature_importances = rf.feature_importances_
    feature_names = X.columns
    importances_df = pd.DataFrame({'Feature': feature_names, 'Importance': feature_importances})
    importances_df = importances_df.sort_values(by='Importance', ascending=False)

    return importances_df


def calc_anova(df_input):
    """
    Performs an ANOVA analysis (Analysis of Variance) for the features.

    Parameters:
    df_input -- Input data as Pandas DataFrame, contains cluster membership

    Returns:
    anova_df -- Panda's DataFrame with the ANOVA results (F-statistic and p-value) for each feature
    """
    anova_results = []
    with warnings.catch_warnings(record=True) as w:
        for feature in df_input.columns[:-1]:
            groups = [df_input[df_input['Cluster'] == cluster][feature] for cluster in df_input['Cluster'].unique()]
            f_stat, p_val = f_oneway(*groups)
            anova_results.append({'Feature': feature, 'F-statistic': f_stat, 'p-value': p_val})
            if len(w) > 0 and w[-1].category.__name__ == "ConstantInputWarning": # Filter warning for one-hot columns with all values being 0
                print(f"Cannot compute ANOVA for {feature}, because all values are equal")

    anova_df = pd.DataFrame(anova_results)
    anova_df = anova_df.sort_values(by='F-statistic', ascending=False)

    return anova_df


# FCM
def calc_transition_counts(df_input1, df_input2):
    """
    Calculates the transitions between clusters at two different points in time.

    Parameters:
    df_input1: Input data at the first point in time as Pandas DataFrame
    df_input2: Input data at the second point in time as Pandas DataFrame

    Return:
    transition_counts: Pandas DataFrame with the transition counts and rates between the clusters
    """
    cluster_transitions = pd.merge(df_input1[['unique_id', 'Cluster']],
                                   df_input2[['unique_id', 'Cluster']], 
                                   on='unique_id', suffixes=('_t1', '_t2'))

    transition_counts = cluster_transitions.groupby(['Cluster_t1', 'Cluster_t2']).size().reset_index(name='count')
    transition_counts['transition_rate'] = transition_counts['count'] / len(df_input1)

    return transition_counts


# FCM
def calc_mult_cluster_distances(cluster_centers_t1, cluster_centers_t2):
    """
    Calculates the distances between the cluster centroids at two different points in time.

    Parameters:
    cluster_centers_t1: centroids of the clusters at the first point in time
    cluster_centers_t2: Centroids of the clusters at the second point in time

    Returns:
    distances_dict: Dictionary with the distances between the centroids
    distance_matrix: Matrix of the distances between the centroids
    """
    distance_matrix = cdist(cluster_centers_t1, cluster_centers_t2, 'euclidean')
    
    distances_dict = {}
    for i in range(distance_matrix.shape[0]):
        for j in range(distance_matrix.shape[1]):
            distances_dict[(i, j)] = distance_matrix[i, j]
    
    return distances_dict, distance_matrix


# FCM
def calc_linear_sum_assignment(distance_matrix):
    """
    Solves the linear assignment problem using the modified Jonker-Volgenant algorithm.

    Parameters:
    distance_matrix: Matrix of distances/costs

    Returns:
    matching: Dictionary with the optimal assignment of the cluster centroids
    """
    row_ind, col_ind = linear_sum_assignment(distance_matrix)

    matching = dict(zip(row_ind, col_ind))

    return matching
