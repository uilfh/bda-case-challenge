import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.graph_objects as go


default_fig_size = (10,6)


def plot_k_distance(data, k):
    """
    Plots the K-nearest neighbor graph.

    Parameters:
    data: The K-nearest neighbor distance data
    k: The number of nearest neighbors

    Returns:
    None
    """
    plt.plot(data)
    plt.title("K Nearest Neighbor Graph")
    plt.xlabel('Data points sorted by distance')
    plt.ylabel(f'{k}-th Nearest Neighbor distance')
    plt.show()
    return


def plot_silhouette_scores_per_eps(eps_values, min_samples, silhouette_scores):
    """
    Plots the silhouette scores for different eps values.

    Parameters:
    eps_values: List of eps values
    min_samples: Number of minimum samples
    silhouette_scores: List of silhouette scores

    Returns:
    None
    """
    plt.figure(figsize=default_fig_size)
    plt.plot(eps_values, silhouette_scores, marker='o')
    plt.title(f'Silhouette Scores for different Eps values with MinPts={min_samples}')
    plt.xlabel('Eps')
    plt.ylabel('Silhouette Score')
    plt.show()
    return


def plot_pca(data, title="Clustering (PCA Projection)"):
    """
    Plots the PCA projection of the clusters.

    Parameters:
    data: DataFrame with PCA1, PCA2 and cluster columns
    title: Title of the plot

    Returns:
    None
    """
    plt.figure(figsize=default_fig_size)
    sns.scatterplot(x='PCA1', y='PCA2', hue='Cluster', data=data, palette='viridis', legend='full')
    plt.title(title)
    plt.show()
    return


def plot_pca_outliers(data, max_membership, outlier_threshold, title="Clustering (PCA Projection) with Outliers"):
    """
    Plots the PCA projection of the clusters and marks outliers.

    Parameters:
    data: DataFrame with PCA1, PCA2 and cluster columns
    max_membership: Membership values of the clusters
    outlier_threshold: Threshold value for identifying outliers
    title: Title of the plot

    Returns:
    None
    """
    plt.figure(figsize=default_fig_size)
    sns.scatterplot(x='PCA1', y='PCA2', hue='Cluster', data=data, palette='viridis', legend='full')
    plt.scatter(data.loc[max_membership < outlier_threshold, 'PCA1'], 
                data.loc[max_membership < outlier_threshold, 'PCA2'], 
                color='red', marker='x', label='Outliers')
    plt.title(title)
    plt.legend()
    plt.show()
    return


def plot_pca_cluster_centers_shift(data):
    """
    Plots the displacement of the cluster centroids in the PCA projection.

    Parameters:
    data -- DataFrame with PCA1, PCA2, and ID columns (ID contains 't1' and 't2' for two points in time)

    Returns:
    None
    """
    data_t1 = data[data["ID"].str.contains("t1")]
    data_t2 = data[data["ID"].str.contains("t2")]

    x1 = data_t1["PCA1"].values
    y1 = data_t1["PCA2"].values
    x2 = data_t2["PCA1"].values
    y2 = data_t2["PCA2"].values

    _, ax = plt.subplots()
    ax.margins(x=0.5, y=1)
    plt.xticks(range(-2, 3, 1))
    plt.yticks(np.arange(-1.5, 2.0, 0.5))
    sns.scatterplot(x='PCA1', y='PCA2', hue='Cluster', data=data, palette='viridis', legend='full')
    ax.quiver(x1, y1, (x2-x1), (y2-y1), angles='xy', scale_units='xy', scale=1.02, width=0.005,
              headwidth=6, headlength=6, color=[l.get_color() for l in plt.legend().parent.get_lines()])
    plt.gcf().set_size_inches(default_fig_size)
    plt.title("FCM Cluster Centers Shift (PCA Projection)")
    plt.show()
    return

def plot_silhouette_graph(silhouette_values, silhouette_avg, cluster_labels, label):
    """
    Plots the silhouette plot for clusters.

    Parameters:
    silhouette_values: Silhouette values for each data point
    silhouette_avg: Average silhouette score
    cluster_labels: Cluster labels for the data
    label: Label of the clusters

    Returns:
    None
    """
    plt.figure(figsize=default_fig_size)
    y_lower = 10
    for i in range(len(np.unique(cluster_labels))):
        ith_cluster_silhouette_values = silhouette_values[cluster_labels == i]
        ith_cluster_silhouette_values.sort()
        size_cluster_i = ith_cluster_silhouette_values.shape[0]
        y_upper = y_lower + size_cluster_i
        plt.fill_betweenx(np.arange(y_lower, y_upper), 0, 
                          ith_cluster_silhouette_values, alpha=0.7)
        y_lower = y_upper + 10  

    plt.title(f'Silhouette plot for the {label} clusters')
    plt.xlabel('Silhouette coefficient values')
    plt.ylabel('Cluster label')
    plt.axvline(x=silhouette_avg, color="red", linestyle="--")
    plt.show()
    return


def plot_feature_importances(data, label="Cluster"):
    """
    Plots the feature importance.

    Parameters:
    data: DataFrame with the features and their importance
    label: Label of the clusters

    Returns:
    None
    """
    plt.figure(figsize=(10,8))
    sns.barplot(x='Importance', y='Feature', data=data, gap=2)
    plt.title(f'{label} Feature Importance')
    plt.show()
    return


def plot_centroid_heatmap(centroid_distances, n_clusters):
    """
    Plots a heatmap of the distances between cluster centroids.

    Parameters:
    centroid_distances: Matrix of the distances between the centroids
    n_clusters: Number of clusters

    Returns:
    None
    """
    plt.figure(figsize=default_fig_size)
    sns.heatmap(centroid_distances, annot=True, fmt=".2f", 
                cmap="viridis", xticklabels=range(n_clusters), 
                yticklabels=range(n_clusters))
    plt.xlabel('Cluster')
    plt.ylabel('Cluster')
    plt.title('FCM Centroid Distances')
    plt.show()
    return


def plot_feature_distributions(df_input, cluster_labels, cols_to_exclude, label="Feature"):
    """
    Plots the distributions of the features for all clusters.

    Parameters:
    df_input: Input data as DataFrame
    cluster_labels: Cluster labels for the data
    cols_to_exclude: Number of columns at the end that are to be excluded
    label: Label of the features

    Returns:
    None
    """
    cluster_dfs = [df_input[df_input["Cluster"] == i] for i in cluster_labels.unique()]

    plt.rcParams.update({'figure.max_open_warning': 0}) # Disable warning for generating many figures

    # Plot the distribution of every feature in all clusters
    for i in cluster_dfs[0].columns[:-cols_to_exclude]:
        comparison = pd.DataFrame([pd.Series(j[i].values) for j in cluster_dfs]).T
        sns.violinplot(comparison, orient="h", density_norm="count")
        #sns.kdeplot(comparison, fill=False, linewidth=2, alpha=1, multiple="layer") # Plots of midterm presentation
        #sns.histplot(comparison, multiple="dodge", stat="percent", shrink=len(comparison.iloc[0].unique())*0.2+1) # Problem with different amount of bars
        plt.xlabel("Value")
        plt.ylabel("Cluster")
        plt.title(f"{label} - {i}")
        plt.figure()
    return


def plot_features_pairwise(df_input, cluster_labels, cols_to_exclude):
    """
    Plots pairwise comparisons of features for each cluster.

    Parameters:
    df_input: Input data as DataFrame
    cluster_labels: Cluster labels for the data
    cols_to_exclude: Number of columns at the end to be excluded

    Returns:
    None
    """
    cluster_dfs = [df_input[df_input["Cluster"] == i] for i in cluster_labels.unique()]

    for i in cluster_dfs:
        print("Cluster {0}:".format(i["Cluster"].values[0]))
        sns.pairplot(i.iloc[:,:-cols_to_exclude], kind="kde", corner=True)
    return


def plot_sankey_diagram(df_input, n_clusters):
    """
    Plots a Sankey diagram for cluster transitions.

    Parameters:
    df_input: Input data as DataFrame with 'Cluster_t1' and 'Cluster_t2'
    n_clusters: Number of clusters

    Returns:
    None
    """
    labels = [f"Cluster {i}_t1" for i in range(n_clusters)] + [f"Cluster {i}_t2" for i in range(n_clusters)]
    sources = df_input['Cluster_t1'].values
    targets = df_input['Cluster_t2'].values + n_clusters  # offset targets for t2
    values = df_input['transition_rate'].values
    dict_amounts = ([x["count"].sum() for x in (df_input[df_input["Cluster_t1"] == i] for i in range(0, n_clusters))] + 
                    [x["count"].sum() for x in (df_input[df_input["Cluster_t2"] == i] for i in range(0, n_clusters))])
    link_amounts = df_input["count"].values

    fig = go.Figure(data=[go.Sankey(
        node=dict(
            pad=15,
            thickness=20,
            line=dict(color="black", width=0.5),
            label=labels,
            customdata=dict_amounts,
            hovertemplate="%{customdata} Data Points"
        ),
        link=dict(
            source=sources,
            target=targets,
            value=values,
            customdata=link_amounts,
            hovertemplate="%{customdata} Data Points"
        )
    )])

    fig.update_layout(title_text="Cluster Transitions from T1 to T2", font_size=10)
    fig.show()
    return
