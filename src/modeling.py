import numpy as np
import pandas as pd
from sklearn.neighbors import NearestNeighbors
from sklearn.cluster import DBSCAN
from sklearn.metrics import silhouette_score
from skfuzzy.cluster import cmeans


def calc_nneighbors_distances(df_input, n_neighbors):
    """
    Calculate the distances to the nearest neighbors for each point in the DataFrame.

    This function fits a NearestNeighbors model to the input DataFrame and calculates
    the distances to the nearest neighbors, excluding the point itself. The distances
    are then sorted.

    Parameters:
    df_input (pd.DataFrame): The input DataFrame containing the data points.
    n_neighbors (int): The number of nearest neighbors to consider.

    Returns:
    np.ndarray: A sorted 1D array of distances to the nearest neighbors.
    """
    nneighbors = NearestNeighbors(n_neighbors=n_neighbors)
    neighbors_fit = nneighbors.fit(df_input)
    distances, _ = neighbors_fit.kneighbors(df_input)
    distances = distances[:, 1:].flatten()  # Remove distances of points to themselves, then transform into 1d-array
    distances = np.sort(distances, axis=0)

    return distances


def find_best_dbscan_params(df_input, eps_values, min_samples_list):
    """
    Find the best DBSCAN parameters based on silhouette scores.

    This function iterates over a range of epsilon and min_samples values, fitting
    a DBSCAN model for each combination and calculating the silhouette score. The
    results are collected and returned.

    Parameters:
    df_input (pd.DataFrame): The input DataFrame containing the data points.
    eps_values (list of float): List of epsilon values to test.
    min_samples_list (list of int): List of min_samples values to test.

    Returns:
    tuple: Three lists containing silhouette scores, epsilon values, and min_samples values.
    """
    silhouette_scores = []
    eps_list = []
    minpts_list = []
    for min_samples in min_samples_list:
        for eps in eps_values:
            dbscan = DBSCAN(eps=eps, min_samples=min_samples)
            clusters = dbscan.fit_predict(df_input)
            if len(set(clusters)) > 1:  # Silhouette score is only defined if there is more than 1 cluster
                score = silhouette_score(df_input, clusters)
                silhouette_scores.append(score)
                eps_list.append(eps)
                minpts_list.append(min_samples)
            else:
                silhouette_scores.append(-1)
                eps_list.append(eps)
                minpts_list.append(min_samples)
    
    return silhouette_scores, eps_list, minpts_list


def calc_dbscan_clusters(df_input, best_eps, best_min_samples):
    """
    Calculate DBSCAN clusters for the input DataFrame.

    This function fits a DBSCAN model with the specified epsilon and min_samples
    values to the input DataFrame and returns the cluster labels.

    Parameters:
    df_input (pd.DataFrame): The input DataFrame containing the data points.
    best_eps (float): The chosen best epsilon value for DBSCAN.
    best_min_samples (int): The chosen best min_samples value for DBSCAN.

    Returns:
    np.ndarray: An array of cluster labels.
    """
    dbscan = DBSCAN(eps=best_eps, min_samples=best_min_samples)
    clusters = dbscan.fit_predict(df_input)

    return clusters


def calc_fcm_clusters(df_input, n_clusters, m, error=0.005, maxiter=1000):
    """
    Calculate Fuzzy C-Means clusters for the input DataFrame.

    This function applies Fuzzy C-Means clustering to the input DataFrame and returns
    the cluster labels, cluster centers, maximum membership values, and the fuzzy
    partition coefficient.

    Parameters:
    df_input (pd.DataFrame): The input DataFrame containing the data points.
    n_clusters (int): The number of clusters to form.
    m (float): The fuzziness parameter.
    error (float): The stopping criterion for the algorithm, default set to 0.005.
    maxiter (int): The maximum number of iterations, default set to 1000.

    Returns:
    tuple: Four elements containing the cluster labels, cluster centers, maximum membership values,
           and the final partition coefficient.
    """
    cluster_centers, fcm_matrix, init_fcm_matrix, dist_matrix, func_hist, iter, fpc = cmeans(
        df_input.T, n_clusters, m, error, maxiter, init=None
    )

    clusters = np.argmax(fcm_matrix, axis=0)
    max_membership = np.max(fcm_matrix, axis=0)

    return clusters, cluster_centers, max_membership, fpc


def calc_pca(df_input, pca, clusters, perform_fitting=True):
    """
    Calculate PCA components for the input DataFrame and assign clusters.

    This function reduces the dimensionality of the input DataFrame using PCA and
    assigns cluster labels to the resulting components. If `perform_fitting` is True,
    the PCA model is fitted to the data; otherwise, it is only transformed.

    Parameters:
    df_input (pd.DataFrame): The input DataFrame containing the data points.
    pca (PCA): An instance of sklearn.decomposition.PCA.
    clusters (np.ndarray): An array of cluster labels.
    perform_fitting (bool): Whether to fit the PCA model to the data or not, default set to True.

    Returns:
    pd.DataFrame: A DataFrame containing the PCA components and cluster labels.
    """
    if perform_fitting:
        pca_components = pca.fit_transform(df_input.drop('Cluster', axis=1, errors="ignore"))
    else:
        pca_components = pca.transform(df_input.drop('Cluster', axis=1, errors="ignore"))

    pca_df = pd.DataFrame(data=pca_components, columns=['PCA1', 'PCA2'])
    pca_df['Cluster'] = clusters

    return pca_df
