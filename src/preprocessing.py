import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder


def normalize_data(df_input, cols_to_ignore):
    """
    Normalize numerical data in a DataFrame, ignoring specified columns.

    This function drops the specified columns from the input DataFrame,
    then normalizes the remaining numerical data to a range of [0, 1] using
    MinMaxScaler.

    Parameters:
    df_input (pd.DataFrame): The input DataFrame containing the data to be normalized.
    cols_to_ignore (list of str): List of column names to be ignored during normalization.

    Returns:
    pd.DataFrame: A DataFrame containing the normalized data, with the ignored columns removed.
    """
    df_input = df_input.drop(cols_to_ignore, axis=1)

    norm_scaler = MinMaxScaler()
    normalized_data = norm_scaler.fit_transform(df_input)
    normalized_features = pd.DataFrame(normalized_data, columns=df_input.columns)

    return normalized_features


def one_hot_encode_data(df_input, categorical_cols):
    """
    One-hot encode specified categorical columns in a DataFrame.

    This function takes the specified categorical columns in the input DataFrame
    and applies one-hot encoding to them, producing a new DataFrame with the
    encoded columns.

    Parameters:
    df_input (pd.DataFrame): The input DataFrame containing the data to be encoded.
    categorical_cols (list of str): List of column names to be one-hot encoded.

    Returns:
    pd.DataFrame: A DataFrame containing the one-hot encoded features.
    """
    categorical_data = df_input[categorical_cols]

    ohe = OneHotEncoder(sparse_output = False)
    ohe_transform = ohe.fit_transform(categorical_data.values.reshape(-1, len(categorical_cols)))
    cols = [i for j in [[str(x[0])+str(x[1]) for x in tups] 
            for tups in [[(tup[0], i) for i in tup[1]] 
            for tup in [(col, range(1, df_input[col].nunique()+1)) 
            for col in categorical_cols]]] for i in j]
    encoded_features = pd.DataFrame(ohe_transform, columns=cols)

    return encoded_features


def generate_weighted_groups(df_input, thematic_groups_dict):
    """
    Generate weighted groups of features from a DataFrame based on thematic groups.

    This function creates weighted groups of features by normalizing each group
    of columns defined in the thematic groups dictionary. The weighting is done
    by dividing each value by the square root of the number of columns in the group.

    Parameters:
    df_input (pd.DataFrame): The input DataFrame containing the data to be grouped.
    thematic_groups_dict (dict): A dictionary where keys are group names and values are
                                 lists of column names belonging to each group.

    Returns:
    pd.DataFrame: A DataFrame containing the weighted groups of features.
    """
    weighted_groups = pd.DataFrame(index=df_input.index)

    for group, columns in thematic_groups_dict.items():
        group_data = df_input[columns]
        group_weight = np.sqrt(group_data.shape[1])
        weighted_group_data = group_data / group_weight
        
        for column in columns:
            column_name = f'{group}_{column}'
            weighted_groups[column_name] = weighted_group_data[column]

    return weighted_groups
